package kr.co.insighton.e3.core.constant;

public enum MessageDisplayType {
	
	NONE, WINDOW_POPUP, ALERT, LAYER_POPUP;

}
