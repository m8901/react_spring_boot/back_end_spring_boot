package kr.co.insighton.e3.test;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kr.co.insighton.e3.test.DTO.ReplyForm;
import kr.co.insighton.e3.test.DTO.Result;
import kr.co.insighton.e3.test.DTO.TestBreplyDTO;
import kr.co.insighton.e3.test.service.TestBreplyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ApiReplyController {

	private final TestBreplyService testBreplyServise;
	//댓글등록
	@PostMapping("/{bnum}")
	public Result<List<TestBreplyDTO>> addReply(@PathVariable int bnum,
							@RequestBody ReplyForm rf,
							HttpServletRequest request) {
		TestBreplyDTO testBreplyDTO = new TestBreplyDTO();
		BeanUtils.copyProperties(rf, testBreplyDTO);
		
		testBreplyDTO.setBnum(bnum);
		log.info("DTO:"+testBreplyDTO);
		
		Result<List<TestBreplyDTO>> result;
		
		List<TestBreplyDTO> list = testBreplyServise.addReply(bnum, testBreplyDTO);
		log.info("list:"+list);
		result = new Result<List<TestBreplyDTO>>("00","성공",list);
		return result;
		
	}
	
	//댓글삭제
	@DeleteMapping("/{rnum}/{bnum}")
	public Result<List<TestBreplyDTO>> delete(@PathVariable int rnum,
												@PathVariable int bnum,
												HttpServletRequest request){
		
		Result<List<TestBreplyDTO>> result;
		
		List<TestBreplyDTO> list = testBreplyServise.delete(rnum, bnum);
		log.info("삭제후 list:"+list);
		result = new Result<List<TestBreplyDTO>>("00","성공",list);
		return result;
		
	}
}
