package kr.co.insighton.e3.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.insighton.e3.demo.domain.Board;
import kr.co.insighton.e3.demo.repository.BoardRepository;

@Service
public class BoardService {
	
	@Autowired
	private BoardRepository boardRepository;

	public List<Board> getList(){
		
		List<Board> result =  boardRepository.getList();
		
		System.out.println("resultdd:"+result);
		return result;
	}
	
	public Board get(int boardSeq) {
		return boardRepository.get(boardSeq);
	}
	
	public void save(Board board) {
		boardRepository.save(board);
	};
	
	public void update(Board board) {
		boardRepository.update(board);
	};
	
	public void delelte(int boardSeq) {
		boardRepository.delelte(boardSeq);
	};
}
