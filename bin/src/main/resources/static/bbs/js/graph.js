// tab_menu
$("document").ready(function() {
       
	$(".tab_content").hide();
	$(".tab_content").eq(0).show();
	
	$(".tab_menu li").click(function(){
		$(".tab_menu li").removeClass("active")
		$(this).addClass("active")
		$(".tab_content").hide();
		var tabid = $(this).attr("rel");
		$("#"+tabid).show();
		wonGraph1();
		wonGraph2();
	})
	
});

// 원그래프 함수
var wonGraph1 = function (n) {
	if (n > 100) n = 100;
	if (n < 0) n = 0;

	var n1 = (n / 100 * 180);
	var n2 = (n / 100 * 360);

	$('.radial_graph_wrap.year .radial_graph').addClass('perZero'); // 0%에서 시작

	setTimeout(function () {
		$('.radial_graph_wrap.year .radial_graph').removeClass('perZero');

		$('.radial_graph_wrap.year .full_mask, .radial_graph_wrap.year .fill').css({
			transform: 'rotate(' + n1 + 'deg)'
		});
		$('.radial_graph_wrap.year .fill.shim').css({
			transform: 'rotate(' + n2 + 'deg)'
		});
	}, 100)
	return false;
};
var wonGraph2 = function (n) {
	if (n > 100) n = 100;
	if (n < 0) n = 0;

	var n1 = (n / 100 * 180);
	var n2 = (n / 100 * 360);

	$('.radial_graph_wrap.month .radial_graph').addClass('perZero'); // 0%에서 시작

	setTimeout(function () {
		$('.radial_graph_wrap.month .radial_graph').removeClass('perZero');

		$('.radial_graph_wrap.month .full_mask, .radial_graph_wrap.month .fill').css({
			transform: 'rotate(' + n1 + 'deg)'
		});
		$('.radial_graph_wrap.month .fill.shim').css({
			transform: 'rotate(' + n2 + 'deg)'
		});
	}, 100)
	return false;
};