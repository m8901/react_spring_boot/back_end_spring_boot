package kr.co.insighton.e3.test;


import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import kr.co.insighton.e3.test.DTO.AddForm;
import kr.co.insighton.e3.test.DTO.ReplyForm;
import kr.co.insighton.e3.test.DTO.TestBoardDTO;
import kr.co.insighton.e3.test.DTO.TestBreplyDTO;
import kr.co.insighton.e3.test.service.TestBoardService;
import kr.co.insighton.e3.test.service.TestBreplyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/test")
@Controller
public class TestBoardController {

	
	@Autowired //TestBoardService testBoardService = new TestBoardService();
	private final TestBoardService testBoardService;
	@Autowired
	private final TestBreplyService testBreplyService;
	
	//게시글 목록 불러오기
	@GetMapping("/")
	public String boardList(Model model) {
		log.info("컨트롤러도착");
		List<TestBoardDTO> list = testBoardService.list();
		
		log.info(list.toString());
		model.addAttribute("list", list);
		
		return "board/boardList";
	}
	//카테고리별 게시글 목록 조회
	@GetMapping("/{bcategory}")
	public String boardList(@PathVariable String bcategory, Model model) {
		List<TestBoardDTO> list = testBoardService.list(bcategory);
		
		log.info(list.toString());
		model.addAttribute("list", list);
		
		return "board/boardList";
	}
	
	//게시글 작성 화면 출력
	@GetMapping("/board/write")
	public String add(@ModelAttribute AddForm addForm) {
		
		return "board/addBoard";
	}
	
	//게시글 작성
	@PostMapping("/board/write")
	public String addBoard(@ModelAttribute AddForm addForm,
							RedirectAttributes redirectAttributes) {
		
		TestBoardDTO testBoardDTO = new TestBoardDTO();
		BeanUtils.copyProperties(addForm, testBoardDTO);
		testBoardService.addPost(testBoardDTO);
		log.info("testBoardDTO:" + testBoardDTO.toString());
		
		redirectAttributes.addAttribute("bnum", testBoardDTO.getBnum());
		
		return "redirect:/test/board/{bnum}";
	}
	//게시글 상세화면 출력
	@GetMapping("/board/{bnum}")
	public String boardDetail(@PathVariable int bnum, Model model,
			  					@ModelAttribute ReplyForm replyForm) {
	  log.info("상세화면 컨트롤러"); TestBoardDTO result
	  =testBoardService.boardDetail(bnum); result.setBnum(bnum);
	  log.info("게시글조회 :"+result.toString());
	  
	  model.addAttribute("post", result);
	  
	  //게시글 조회시 댓글도 같이 불러오기
	  List<TestBreplyDTO> list = testBreplyService.list(bnum);
	  log.info("댓글조회 :"+list.toString());
	  model.addAttribute("reply",list);
	  
	  return "board/boardDetail"; 
	  }
	 
	

	//게시글 수정화면 출력
	  @GetMapping("/modi/{bnum}") 
	 public String modiPost(@PathVariable int bnum,Model model) {
	  
	  TestBoardDTO testBoardDTO = new TestBoardDTO(); 
	  testBoardDTO = testBoardService.boardDetail(bnum);
	  
	  model.addAttribute("post", testBoardDTO);
	  
	  return "board/modifyBoard"; 
	  }
	 
	
	//게시글 수정
	@PostMapping("/modi/{bnum}")
	public String modiPost(@PathVariable int bnum,
							@ModelAttribute AddForm addForm,
							RedirectAttributes redirectAttributes) {
		
		TestBoardDTO testBoardDTO = new TestBoardDTO();
		BeanUtils.copyProperties(addForm, testBoardDTO);
		testBoardDTO.setBnum(bnum);
		testBoardService.modiPost(testBoardDTO);
		log.info("수정된게시글 :" + testBoardDTO.toString());
		
		redirectAttributes.addAttribute("bnum",testBoardDTO.getBnum());
		
		return "redirect:/test/board/{bnum}";
	}
	
	//게시글 삭제
	@GetMapping("/del/{bnum}")
	public String delPost(@PathVariable int bnum) {
		
		testBoardService.delPost(bnum);
		
		return "redirect:/test/";
	}
	
	//댓글 추가

}
