package kr.co.insighton.e3.smhBoard;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class NowDateUtil {
	public static String getDate() { 
		
		LocalDate now = LocalDate.now();
		  
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		  
		String date = now.format(formatter);
		 
		return date; 
	}

}
