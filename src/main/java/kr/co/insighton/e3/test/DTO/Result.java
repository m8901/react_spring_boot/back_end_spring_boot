package kr.co.insighton.e3.test.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result<T> {
	private String rtcd;
	private String rtmsg;
	private T data;
}
