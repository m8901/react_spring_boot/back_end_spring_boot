
$(document).ready(function(){
	$(".depth").hide();
	$(".choice").hide();
	$(".apply").hide();
	
	$('.depth_1 input:radio').click(function(){
		var inputValue = $(this).attr("value");
		var targetBox = $("." + inputValue);
		$(".apply").hide();
		$(".depth_3").hide();
		$(".depth_4_1").hide();
		$(".depth_4_2").hide();
		if(inputValue == "depth_sub_가"){
			$(".depth_2").hide();
			$(".depth_sub").hide();
			$(".depth_2").show();
			$(".depth_2 .choice").hide();
			$(".choice_가").show();
		}else if(inputValue == "depth_sub_나"){
			$(".depth_2").hide();
			$(".depth_sub").hide();
			$(".depth_2").show();
			$(".depth_2 .choice").hide();
			$(".choice_나").show();
		}else{
			$(".depth_sub").hide();
			$(".depth_2").show();
			$(".depth_2 .choice").not(targetBox).hide();
		}
		$(targetBox).show();
    });
	/*
	$('.depth_sub_가 input:radio').click(function(){
		$(".depth_2").show();
		$(".depth_2 .choice").hide();
		$(".choice_가").show();
    });
	$('.depth_sub_나 input:radio').click(function(){
		$(".depth_2").show();
		$(".depth_2 .choice").hide();
		$(".choice_나").show();
    });
	*/
	$('.depth_2 input:radio').click(function(){
		var inputValue = $(this).attr("value");
		var targetBox = $("." + inputValue);
		$(".apply").hide();
		$(".depth_4_1").hide();
		$(".depth_4_2").hide();
		$(".depth_3").show();
		$(".depth_3 .choice").not(targetBox).hide();
		$(targetBox).show();
		if(inputValue.substring(0,8) == 'choice_다'){
			$(".depth_4_1").hide();
			$(".depth_4_2").show();
		}else{
			$(".depth_4_2").hide();
			$(".depth_4_1").show();
		}
    });
	$('.apply_check input:radio').click(function(){
		var inputValue = $(this).attr("value");
		if(inputValue == 'apply'){
			$(".apply").show();
		}else{
			$(".apply").hide();
		}
    });
});