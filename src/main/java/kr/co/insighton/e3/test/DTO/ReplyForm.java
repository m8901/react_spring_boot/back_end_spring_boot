package kr.co.insighton.e3.test.DTO;

import lombok.Data;

@Data
public class ReplyForm {

	private String nickname;
	private String rcontent;
	
}
