package kr.co.insighton.e3.test.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import kr.co.insighton.e3.test.DTO.TestBreplyDTO;

@Mapper
public interface TestBreplyRepository {

	//전체댓글조회
	List<TestBreplyDTO> list(int bnum);
	
	//댓글등록
	void addReply(int bnum,@Param("reply") TestBreplyDTO testBreplyDTO);
	
	//댓글삭제
	void delete(int rnum);
}
