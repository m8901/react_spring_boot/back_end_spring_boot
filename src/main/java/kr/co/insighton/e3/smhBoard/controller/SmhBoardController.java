package kr.co.insighton.e3.smhBoard.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import kr.co.insighton.e3.smhBoard.NowDateUtil;
import kr.co.insighton.e3.smhBoard.DTO.SmhBoardDTO;
import kr.co.insighton.e3.smhBoard.DTO.SmhBoardReDTO;
import kr.co.insighton.e3.smhBoard.service.SmhBoardService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/smhBoard")
@Controller
public class SmhBoardController {

	@Autowired 
	private SmhBoardService smhBoardService;
	 
	 
	//게시판 목록
	@GetMapping("/list")
	public String boardList(Model model) {
		log.info("게시판 컨트롤러");
		List<SmhBoardDTO> boardList = smhBoardService.list();
		
		model.addAttribute("boardList", boardList);
		
		return "smhBoard/smhBoard_list";
	}
	
	//카테고리 구분 목록 조회
	@GetMapping("/{boardCate}")
	public String boardList(Model model, @PathVariable String boardCate) {
		log.info("카테고리 구분");
		List<SmhBoardReDTO> listCate = smhBoardService.selectListCate(boardCate);
		
		model.addAttribute("boardList" ,listCate);
		
		return "smhBoard/smhBoard_list";
	}
	
	//게시판 작성 페이지로 이동
	@GetMapping("/writeForm")
	public String goWriteForm(Model model) {
		log.info("작성 페이지 컨트롤러");
		
		model.addAttribute("date", NowDateUtil.getDate());
		
		return "smhBoard/smhBoard_write";
	}
	
	//게시글 등록
	@PostMapping("/insertBoard")
	public String insertBoard(SmhBoardDTO smhBoardDTO) {
		log.info("게시글 등록");
		
		smhBoardService.insertBoard(smhBoardDTO);
		
		return "redirect:/smhBoard/list";
	}
	
	//게시판 상세보기
	@GetMapping("/detail/{boardCode}")
	public String boardDetail(Model model, @PathVariable int boardCode, SmhBoardReDTO smhBoardReDTO) {
		log.info("상세화면 컨트롤러");
		smhBoardService.updateCnt(boardCode);
		
		SmhBoardDTO result = smhBoardService.selectDetail(boardCode);

		result.setBoardCode(boardCode);
		
		model.addAttribute("board", result);
		
		//댓글 조회
		model.addAttribute("date", NowDateUtil.getDate());
		
		List<SmhBoardReDTO> replyList = smhBoardService.replyList(smhBoardReDTO);
		model.addAttribute("reply", replyList);
		
		return "smhBoard/smhBoard_detail";
	}
	
	//게시판 삭제
	@GetMapping("/deleteBoard/{boardCode}")
	public String boardDelete(@PathVariable int boardCode) {
		log.info("게시글 삭제");
		
		smhBoardService.deleteBoard(boardCode);
		
		return "redirect:/smhBoard/list";
	}
	
	//게시판 수정으로 이동
	@GetMapping("/updateBoard/{boardCode}")
	public String goUpdateBoard(Model model, @PathVariable int boardCode) {
		log.info("게시글 수정 페이지");
		model.addAttribute("date", NowDateUtil.getDate());

		SmhBoardDTO result = smhBoardService.selectDetail(boardCode);
		
		result.setBoardCode(boardCode);
		
		model.addAttribute("board", result);
		
		return "smhBoard/smhBoard_modify";
	}
	
	//게시판 수정 등록
	@PostMapping("/updateBoard")
	public String updateBoard(SmhBoardDTO smhBoardDTO, RedirectAttributes re) {
		log.info("게시글 수정 등록");
		
		smhBoardService.updateBoard(smhBoardDTO);
		
		
		re.addAttribute("boardCode", smhBoardDTO.getBoardCode());
		
		return "redirect:/smhBoard/detail/{boardCode}";
	}
	
	//--------- 댓글 영역 ---------\\
	
	//댓글 등록
	@PostMapping("/insertReply") 
	public String insertReply(SmhBoardReDTO smhBoardReDTO, RedirectAttributes re) { 
	
		log.info("댓글 등록");
		  
		smhBoardService.insertReply(smhBoardReDTO);

		re.addAttribute("boardCode", smhBoardReDTO.getBoardCode());
		
		return "redirect:/smhBoard/detail/{boardCode}"; 
	
	}
	
	//댓글 삭제
	@RequestMapping("deleteRe/{replyCode}/{boardCode}")
	public String deleteReply(@PathVariable int replyCode, @PathVariable int boardCode, 
								RedirectAttributes re) {
		log.info("댓글 삭제");
		
		smhBoardService.deleReply(replyCode);
	
		re.addAttribute("boardCode", boardCode);
	
		return "redirect:/smhBoard/detail/{boardCode}";
		
		
	}
}
