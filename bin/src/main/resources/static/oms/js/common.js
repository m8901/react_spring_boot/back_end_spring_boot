$(function () {
  // INIT
  $.fn.device();
  $.fn.gnbSize();
  
  navi();
  sticky();
  small_nav();

  // GNB PC
  function navi() {
    $("#gnb").on("mouseenter", "> .box > ul > li > p", function () {
      if ($("body").data("device") != "mobile") {
        $this = $(this).parent("li");
		$this.addClass("height");
        $this.css("height", "56px").siblings().css("height", "auto");
        $this.parents(".h_group").addClass("menu_hover");
        //$("#gnb > .box > ul > li > a").css("color", "#333");
		
		if ($("#gnb > .box > ul > li:nth-child(1)").hasClass("height")) {
		  $this.parents(".h_group").stop().animate({ height: "190px" }, 300);
		}else if ($("#gnb > .box > ul > li:nth-child(2)").hasClass("height")) {
		  $this.parents(".h_group").stop().animate({ height: "280px" }, 300);
		}else if ($("#gnb > .box > ul > li:nth-child(3)").hasClass("height")) {
		  $this.parents(".h_group").stop().animate({ height: "235px" }, 300);
		}else if ($("#gnb > .box > ul > li:nth-child(4)").hasClass("height")) {
		  $this.parents(".h_group").stop().animate({ height: "280px" }, 300);
		}else if ($("#gnb > .box > ul > li:nth-child(5)").hasClass("height")) {
		  $this.parents(".h_group").stop().animate({ height: "255px" }, 300);
		}else if ($("#gnb > .box > ul > li:nth-child(6)").hasClass("height")) {
		  $this.parents(".h_group").stop().animate({ height: "350px" }, 300);
		}else if ($("#gnb > .box > ul > li:nth-child(7)").hasClass("height")) {
		  $this.parents(".h_group").stop().animate({ height: "203px" }, 300);
		}else {
		  $this.parents(".h_group").stop().animate({ height: "157px" }, 300);
		}
		
        $("#gnb .sub_menu").hide();
		$this.removeClass("height");
        // depth
		//$(this).children("a").css("color", "#e0002a");
        $this.find(".sub_menu").show();
      }
    });

    $(".h_group").on("mouseleave", function () {
      if ($("body").data("device") != "mobile") {
        $("#gnb > .box > ul > li").css("height", "auto");
        $("#gnb > .box > ul > li")
          .parents(".h_group")
          .stop()
          .animate({ height: "56px" }, 300, function () {
            $(this).removeClass("menu_hover");
            $("#gnb > .box > ul > li").siblings().children(".sub_menu").hide();

            if ($(".h_group").hasClass("affix")) {
              $("#gnb > .box > ul > li > a").css("color", "#333");
            } else {
              if (!$(".h_group").hasClass("sub")) {
              }
              $("#gnb > .box > ul > li > a").css("color", "#333");
            }
          });
      }
    });

    // GNB 키보드 접근
    $("#gnb").on("focusin", "> .box > ul > li > p", function () {
      if ($("body").data("device") != "mobile") {
        if ($(".h_group").hasClass("on") == false) {
          $(this).parents(".h_group").addClass("menu_hover");
          $("#gnb > .box > ul > li > a").css("color", "#333");
          $(this).parents(".h_group").stop().animate({ height: "190px" }, 300);
          $("#gnb").find(".sub_menu").hide();
          $(this).next(".sub_menu").show();
        }
      }
    });

    //gnb color on & off
    $("#gnb .gnb_m > li")
      .bind("mouseenter focusin", function () {
        var index = $("#gnb .gnb_m > li").index($(this));
        $("#gnb .gnb_m")
          .find(" > li:eq(" + index + ") a")
          .addClass("on");
      })
      .bind("mouseleave focusout", function () {
        var index = $("#gnb .gnb_m > li").index($(this));
        $("#gnb .gnb_m")
          .find(" > li:eq(" + index + ") a")
          .removeClass("on");
      });

    // h_group 조정
    $(".util_menu_pc").on("focusin", "a", function () {
      $(".h_group").stop().animate({ height: "80px" }, 300);
      $("#gnb").find(".sub_menu").hide();
    });

    $(document).on(
      "focus",
      ".h_group h1 a, .lnb-nav li a, .slick-prev",
      function () {
        if ($("body").data("device") != "mobile") {
          $("#gnb > .box > ul > li")
            .parents(".h_group")
            .stop()
            .animate({ height: "80px" }, 300, function () {
              $("#gnb > .box > ul > li")
                .siblings()
                .children(".sub_menu")
                .hide();
            });
        }
      }
    );
  }

  // Sticky
  function sticky() {
    var fixed_offset = $("#header").offset();

    // alert(fixed_offset.top);

    $(window).on(
      "scroll",
      $.throttle(1000 / 15, function () {
        if ($(document).scrollTop() > fixed_offset.top) {
          $(".h_group").addClass("affix");
          $("#gnb > .box > ul > li > a").css("color", "#333");
        } else {
          $(".h_group").removeClass("affix");
          $("#gnb > .box > ul > li > a").css("color", "#333");
        }
      })
    );
  }

  // GNB MOBILE
  function small_nav() {
    if ($("body").data("device") == "mobile") {
      $("#gnb > .box").css("display", "none");
    }
    $(".btn_menu").on("click", function () {
      var overflowChk = $("body").css("overflow");
      var deviceHeight = $(window).height();

      if (overflowChk == "hidden") {
        $("body").css({
          overflow: "visible",
          height: deviceHeight,
        });
      } else {
        $("body").css({
          overflow: "hidden",
          height: deviceHeight,
        });
      }

      $("#gnb > .box").css("display", "block").addClass("active");
      $(this).next().stop().animate({ right: "0%" }, 500);
      $("#gnb > .dim").fadeIn();

      $("#gnb .btn_menu").fadeOut();
      $("#gnb .btn_close").focus();

      $(".container *, .container + section").on("focusin", function () {
        $("#gnb .btn_close").focus();
      });
    });

    // 모바일 GNB
    $(".gnb_m > li > .depth_1").on("click", function () {
      if (document.body.clientWidth < 1300) {
        $(this).parent("li").stop().toggleClass("current");
        // 3depth

        $(".gnb_m > li > .depth_1")
          .not(this)
          .parent("li")
          .stop()
          .removeClass("current");
        $(this).next(".sub_menu").find(".depth_2").stop().slideToggle();
        $(".gnb_m > li > .depth_1")
          .not(this)
          .next(".sub_menu")
          .find(".depth_2")
          .stop()
          .slideUp();

		if(!$('.wrap').hasClass('en')) {
			$(".gnb_m > li > .depth_1").find('a').attr('title','클릭 시, 하위메뉴 열림');
		} else {
			$(".gnb_m > li > .depth_1").find('a').attr('title','Open submenu on click');
		}

		if($(this).parent("li").hasClass('current')) {
			if(!$('.wrap').hasClass('en')) {
				$(this).find('a').attr('title','클릭 시, 하위메뉴 접힘');
			} else {
				$(this).find('a').attr('title','Close submenu on click');
			}
		} else {
			if(!$('.wrap').hasClass('en')) {
				$(this).find('a').attr('title','클릭 시, 하위메뉴 열림');
			} else {
				$(this).find('a').attr('title','Open submenu on click');
			}
		}
      }
    });

    $(".gnb_m").find(".depth_2 > li > a").on("click", function () {
        if (document.body.clientWidth < 1300) {
			if ($(this).next("ul").hasClass("depth_3")) {

				if($(this).parent("li").hasClass('current')) {
					$(this).parent("li").removeClass('current');
					$(this).next(".depth_3").slideUp();
					if(!$('.wrap').hasClass('en')) {
						$(this).attr('title','클릭 시, 하위메뉴 열림');
					} else {
						$(this).attr('title','Open submenu on click');
					}
				} else {
					$(this).parent("li").addClass('current');
					$(this).next(".depth_3").slideDown();
					if(!$('.wrap').hasClass('en')) {
						$(this).attr('title','클릭 시, 하위메뉴 접힘');
					} else {
						$(this).attr('title','Close submenu on click');
					}
				}
			}
        }
    });

    $("#gnb").on("click", "> .dim, > .box > .btn_close", function () {
      if (document.body.clientWidth < 1280) {
        $("body").css("overflow", "visible");
        $("#gnb > .dim").hide();
        $("#gnb > .box")
          .stop()
          .animate({ right: "-100%" }, 600, function () {
            $("#gnb > .box").css("display", "none").removeClass("active");
          });
        $("#gnb .btn_menu").fadeIn();
        $("#gnb .btn_menu").focus();
        // 모바일 메뉴 초기화
        $(".gnb_m > li").removeClass("current");
        $(".gnb_m > [class^='depth_']").slideUp();
      }
    });
	
  }
});

$.fn.title = function() {
	if (document.body.clientWidth > 1279) {
		$('.gnb_m .depth_2 > li > a').each(function() {
			if ($(this).next("ul").hasClass("depth_3")) {
				var text = $(this).text();
				$(this).replaceWith('<em>' + text + '</em>');
			}
		});
	} else {
		$('.gnb_m .depth_2 > li > em').each(function() {
			var text = $(this).text();
			$(this).replaceWith('<a href="javascript:;">' + text + '</a>');
		});
		if(!$('.wrap').hasClass('en')) {
			$('.gnb_m .depth_1 > a').attr('title','클릭 시, 하위메뉴 열림');
		} else {
			$('.gnb_m .depth_1 > a').attr('title','Open submenu on click');
		}
		$('.gnb_m .depth_2 > li').each(function() {
			if ($(this).find('a').next("ul").hasClass("depth_3")) {
				if(!$('.wrap').hasClass('en')) {
					$(this).find('a').attr('title','클릭 시, 하위메뉴 열림');
				} else {
					$(this).find('a').attr('title','Open submenu on click');
				}
			}
		});
	}
};

// DEVICE CHK
$.fn.device = function () {
  var size = $(window).width() + 17; // 스크롤바 width 추가

  if (size <= 1300) {
    $("body").data("device", "mobile");
  } else {
    $("body").data("device", "pc");
  }
};

// GNB SETTING
$.fn.gnbSize = function () {
  var deviceWidth = $(window).width();
  var deviceHeight = $(window).height();

  if ($("body").data("device") == "mobile") {
    $("body").css("overflow", "visible");
    $("#gnb > .box").css({
      height: deviceHeight});
    $("#gnb .sub_menu").show();
    $("#gnb .sub_menu ul.depth_2").hide();
    if ($("#gnb > .dim").length == 0) {
      $("#gnb").append(
        "<div class='dim' style='display:none;position:fixed;top:0px;left:0px;z-index:10;width:" +
          (deviceWidth + 17) +
          "px;height:" +
          deviceHeight +
          "px;background:#000;filter:alpha(opacity=50);opacity:0.5'></div>"
      );
    }
  } else {
    $("body").css("overflow", "visible");
    $("#header .h_group > div > h1 > a > img").css("display", "block");

    $("#gnb > div.box").css({
      display: "block",
      height: "auto",
    });
    $("#gnb > div.box").css("right", "-100%");
    $("#gnb > div.box > ul > li").removeClass("current");
    $("#gnb .sub_menu").hide();
    $("#gnb .sub_menu ul.depth_2").show();
    $("#gnb .sub_menu > div .inner > ul").show();
    $("#gnb > .dim").remove();
  }
};

// GNB(SiteMap_접근성)
$(function () {
  $(".navbar_toggle").click(function () {
    $(".navbar_toggle").toggleClass("navbar_on");
    $("nav").stop().slideToggle("1000");
    $("nav").removeClass("nav_hide");

	$('.sitemap a:last').on('focus', function()  {
		$(this).keydown(function(e)  {
			if(e.keyCode == 9 && !e.shiftKey) {
				e.preventDefault();
				$('.navbar_on a').focus();
			}
		});
	});
	$('.navbar_on a').on('focus', function(){
		$(this).keydown(function(e)  {
			if(e.keyCode == 9) {
				if(e.shiftKey) {
					e.preventDefault();
					$('.sitemap a:last').focus();
				}
			}
		});
	});

    if ($(".navbar_toggle").hasClass("eng")) {
      $(".navbar_toggle > a").contents()[0].textContent = "Open Whole Menu";
      $(".navbar_toggle > a > div.menu_bar").attr({ title: "Open Whole Menu" });
    } else {
      $(".navbar_toggle > a").contents()[0].textContent = "전체 메뉴 열기";
      $(".navbar_toggle > a > div.menu_bar").attr({ title: "전체 메뉴 열기" });
    }
    if ($(this).hasClass("navbar_on")) {
      $(".nav nav").css("overflow-y", "hidden");

      if ($(".navbar_toggle").hasClass("eng")) {
        $(".navbar_toggle > a").contents()[0].textContent = "Close Whole Menu";
        $(".navbar_toggle > a > div.menu_bar").attr({
          title: "Close Whole Menu",
        });
      } else {
        $(".navbar_toggle > a").contents()[0].textContent = "전체 메뉴 닫기";
        $(".navbar_toggle > a > div.menu_bar").attr({
          title: "전체 메뉴 닫기",
        });
      }
    } else {
      $(".nav nav").css("overflow-y", "hidden");
    }
  });
});

// 왼쪽메뉴 lnb
$(function () {
	$("#lnb .depth_2 > li > a").on("click", function () {
        $(this).parent("li").stop().toggleClass("current");
        $(this).not(this).parent("li").stop().removeClass("current");
        $(this).next(".depth_3").stop().slideToggle();
        $(this).not(this).next(".depth_3").stop().slideUp();
    });
});
// 약관동의
$(function () {
	$(".agree > li > .agree_content").stop().slideUp();
	$(".agree > li > a").on("click", function () {
        $(this).parent("li").stop().toggleClass("current");
        $(this).not(this).parent("li").stop().removeClass("current");
        $(this).next(".agree_content").stop().slideToggle();
        $(this).not(this).next(".agree_content").stop().slideUp();
    });
});
// 방문현황-경비실전용 > 팝업(상세)
$(function () {
	$(".slide li > h4").on("click", function () {
        $(this).parent("li").stop().toggleClass("current");
        $(this).not(this).parent("li").stop().removeClass("current");
        $(this).next(".slide_content").stop().slideToggle();
        $(this).not(this).next(".slide_content").stop().slideUp();
    });
});