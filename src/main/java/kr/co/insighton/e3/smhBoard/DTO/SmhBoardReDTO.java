package kr.co.insighton.e3.smhBoard.DTO;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SmhBoardReDTO {
	private int replyCode;
	private String replyWriter;
	private String replyContent;
	private String replyDate;
	private int boardCode;
}
