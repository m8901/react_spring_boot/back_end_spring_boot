
//게시글 삭제
const $bBtn = document.querySelector("#delBoardBtn")
$bBtn?.addEventListener('click',function(e){
	if(confirm("게시글을 삭제하시겠습니까?")){
		const boardCode = e.target.dataset.bcode;
		location.href=`/smhBoard/deleteBoard/${boardCode}`
	}
})


//댓글 삭제
function reDele(replyCode, boardCode){
	
	if(confirm("댓글을 삭제하시겠습니까?")){
		location.href=`/smhBoard/deleteRe/${replyCode}/${boardCode}`
	}
	
}
