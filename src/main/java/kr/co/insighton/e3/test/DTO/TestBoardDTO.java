package kr.co.insighton.e3.test.DTO;

import java.sql.Date;
import java.time.LocalDate;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TestBoardDTO {

	public int bnum; //게시글번호
	public String bcategory; //게시글카테고리
	public String btitle; //글제목
	public String bcontent; //글본문
	public String nickname; //닉네임
	public String bcdate; //작성시간
	public String budate; //수정시간
	public int bhit; //조회수
}
