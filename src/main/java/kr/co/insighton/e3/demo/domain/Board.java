package kr.co.insighton.e3.demo.domain;

import java.util.Date;

import lombok.Data;

@Data
public class Board {

	private int boardSeq;
	private String titile;
	private String contents;
	private Date regDate;
	
}
