package kr.co.insighton.e3.demo.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import kr.co.insighton.e3.demo.domain.Board;

@Repository
public interface BoardRepository {

	List<Board> getList();
	
	Board get(int boardSeq);
	
	void save(Board board);
	
	void update(Board board);
	
	void delelte(int boardSeq);
	
}
