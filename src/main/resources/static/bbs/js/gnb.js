// gnb
$(function(){
	$(".gnb_open").click(function(){
		$("#gnb").addClass("open");
		$("#gnb .dim").animate({opacity:1},"swing")
		$("#gnb .inner").animate({left:0},500,"swing");
		return false;
	});
	$(".gnb_close").click(function(){
		$("#gnb .dim").animate({opacity:0},300,"swing");
		$("#gnb .inner").animate({left:"-100%"},500,function(){
			$("#gnb").removeClass("open");
		});	
		return false;
	});
});

//go
$(document).ready(function(){
    $('#wrap').scroll(function(){
        var scrollT = $(this).scrollTop();
        var scrollH = $(this).height();
        var contentH = $('#container').height();
        if(scrollT + scrollH - 82 >= contentH) {
            $(".go").addClass("none");
        }else{
			$(".go").removeClass("none");
		}
    });
});