package kr.co.insighton.e3.test.DTO;

import lombok.Data;

@Data
public class AddForm {

	public String bcategory;
	public String nickname;
	public String btitle;
	public String bcontent;
	
}
