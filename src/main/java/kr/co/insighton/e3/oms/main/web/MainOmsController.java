package kr.co.insighton.e3.oms.main.web;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import kr.co.insighton.e3.core.vo.RestResponse;
import kr.co.insighton.e3.oms.main.domain.service.MainOmsService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class MainOmsController {
	
	@Autowired
	private MainOmsService mainOmsService;

	/*-====================================================
	 | VIEW
	 |-====================================================*/
	@GetMapping(value = "/oms/main")
	public ModelAndView viewMainList() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("oms/main");
		return modelAndView;
	}

	@GetMapping(value = "/oms/sub5_1_1방문신청")
	public ModelAndView viewSub5_1_1() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("oms/sub5_1_1방문신청");
		return modelAndView;
	}
	
	@GetMapping(value = "/oms/sub5_1_2방문신청확인(방문자용)")
	public ModelAndView viewSub5_1_2() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("oms/sub5_1_2방문신청확인(방문자용)");
		return modelAndView;
	}

	/*-====================================================
	 | REST API
	 |-====================================================*/
	

}
