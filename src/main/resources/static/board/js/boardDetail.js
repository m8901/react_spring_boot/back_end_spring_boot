const $listBtn = document.querySelector(".detail__listBtn")
const $replyBtn = document.querySelector(".breply_addBtn")

$listBtn?.addEventListener('click',function(){
	window.location.href='/test/';
})

const $delBtn = document.querySelector(".detail__delBtn")
$delBtn?.addEventListener('click',function(e){
	if(confirm("삭제하시겠습니까?")){
		const bnum = e.target.dataset.bnum;
		window.location.href=`/test/del/${bnum}`
	}
})

const $modiBtn = document.querySelector(".detail__modiBtn")
$modiBtn?.addEventListener('click',function(e){
	const bnum = e.target.dataset.bnum;
	window.location.href=`/test/modi/${bnum}`
	
})

/*댓글*/

	const $bnum = document.querySelector('div.board__detail__container').dataset.bnum; //게시글번호
	const $nickname =document.querySelector('.breply__add_nickname');	//닉네임
	const $rcontent = document.querySelector('.breply__add_content');	//댓글본문
	const $container = document.querySelector('.breply__wrap');		//댓글툴
	
	let reModiBtns = document.querySelectorAll('.breply__modiBtn');	//댓글수정버튼
	let reDelBtns = document.querySelectorAll('.breply__delBtn');	//댓글삭제버튼
	
	const addBtn_f =function(){
		const URL = `/api/${$bnum}`;
		const data = {
					"nickname" :$nickname.value,
					"rcontent" :$rcontent.value
		};
		
		request.post(URL,data)
				.then(res=>res.json())
				.then(res=>{
					if(res.rtcd == '00'){
						const data = res.data;
						refreshReply(data);
					}else{
							throw new Error(res.rtmsg);
					}
				})
				.catch(err=>{
					console.log(err.message);
					alert(err.message);
				});
	};
	
	const delBtn_f = function(e){
		
		if(!confirm(`삭제하시겠습니까?`)) return;
		const $rnum = e.target.dataset.rnum;
		
		const URL = `/api/${$rnum}/${$bnum}`;
		
	request.delete(URL)
		.then(res=>res.json())
		.then(res=>{
			if(res.rtcd == '00'){
				//성공로직처리
				const data = res.data;
				//댓글목록갱신
				refreshReply(data);
			}else{
				throw new Error(res.rtmsg);
			}
		})
		.catch(err=>{
			//오류로직 처리
			console.log(err.message);
			alert(err.message);
	});
};
	
	
	$replyBtn.addEventListener('click',addBtn_f);
	
	Array.from(reDelBtns).forEach(ele=>{
		ele.addEventListener('click',delBtn_f);
	})
	
	function refreshReply(data){
		let html ='';
		data.forEach(rec=>{
			//const $rcdate = getTimeStamp(rec.rcdate);
			
			html += `<div class="breply__container">`;
			html += ` <div class="breply__info">`;
			html += `<div class="breply__nickname">${rec.nickname}</div>`;
			html += `<div class="breply__cdate">${rec.rcdate}</div>`;
			html += `</div>`;
			html += `<div class="breply__contentWrap">`;
			html += `<div class="breply__content">${rec.rcontent}</div>`;
			html += `<button class="breply__modiBtn" data-rnum ="${rec.rnum}">수정</button >`;
			html += `<button class="breply__delBtn" data-rnum ="${rec.rnum}">삭제</button >`;
			html += `</div>`;
			html += `</div>`;
			
		});
		$container.innerHTML = html;
		
		//댓글목록 갱신후 생긴 버튼에 이벤트 리스너 달기
		reDelBtns = document.querySelectorAll('.breply__delBtn');
		Array.from(reDelBtns).forEach(ele =>{
			ele.addEventListener('click',delBtn_f);
		});
		$nickname.value='';
		$rcontent.value='';
		
	}