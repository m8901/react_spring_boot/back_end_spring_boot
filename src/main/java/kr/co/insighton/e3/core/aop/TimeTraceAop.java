package kr.co.insighton.e3.core.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TimeTraceAop {

	
//	@Around("execution(* hello.hellospring..*(..))")
//	public Object execute(ProceedingJoinPoint joinPoint) throws Throwable {
//	}
	
	
//	@Around("execution(* kr.co.insighton..*(..))")
//	public Object execute(ProceedingJoinPoint joinPoint) throws Throwable {
//		
//		long start = System.currentTimeMillis();
//		System.out.println("START: "+ joinPoint.toString());
//		try {
//			return joinPoint.proceed();
//		} finally {
//			long finish = System.currentTimeMillis();
//			long timeMs = finish - start;
//			System.out.println("end: " + joinPoint.toString() + " " + timeMs + "ms");
//		}
//	}
}
