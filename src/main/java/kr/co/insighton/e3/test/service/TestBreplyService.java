package kr.co.insighton.e3.test.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.insighton.e3.test.DTO.TestBreplyDTO;
import kr.co.insighton.e3.test.repository.TestBreplyRepository;

@Service
public class TestBreplyService {

	@Autowired
	private TestBreplyRepository testBreplyRepository;
	
	//댓글목록
	public List<TestBreplyDTO> list(int bnum){
		
		List<TestBreplyDTO> list = testBreplyRepository.list(bnum);
		
		return list;
	}
	
	//댓글작성

	public List<TestBreplyDTO> addReply(int bnum,TestBreplyDTO testBreplyDTO) {
		 testBreplyRepository.addReply(bnum, testBreplyDTO);
		 return testBreplyRepository.list(bnum);
	}
	
	//댓글삭제
	public List<TestBreplyDTO> delete(int rnum,int bnum) {
		testBreplyRepository.delete(rnum);
		return testBreplyRepository.list(bnum);
	}
}
