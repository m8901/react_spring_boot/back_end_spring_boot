package kr.co.insighton.e3.sys.code.domain.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.co.insighton.e3.sys.code.domain.repository.CodeMapper;

@Service
@Transactional
public class CodeService {
	
	private final JdbcTemplate jdbcTemplate;
	
	@Autowired
	public CodeService(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Autowired
	private CodeMapper codeMapper;
	
	public Map<String, Object> codeList(Map<String,Object> param){
		Map<String, Object> result = new HashMap<>();
		
		List<Map<String, Object>> inMaster = codeMapper.codeList(param);
		
		System.out.println("inMaster:"+inMaster);
		result.put("result", inMaster);
		
		return result;
	}
	
//	@Transactional
	public Map<String, Object> codeFind(Map<String,Object> param){
		
		Map<String, Object> result = codeMapper.codeFind(param);
		return result;
	}
	
	public Map<String, Object> create(Map<String,Object> param){
		Map<String, Object> result = new HashMap<>();
		
		try {
			codeMapper.save(param);
			result.put("result", "OK");
		}catch(Exception e) {
			result.put("result", "FAIL");
			result.put("MSG",e.toString());
		}
		
		return result;
	}
	
	public Map<String, Object> delete(Map<String,Object> param){
		Map<String, Object> result = new HashMap<>();
		
		try {
			codeMapper.delete(param);
			result.put("result", "OK");
		}catch(Exception e) {
			result.put("result", "FAIL");
			result.put("MSG",e.toString());
		}
		
		return result;
	}
	
	

}
