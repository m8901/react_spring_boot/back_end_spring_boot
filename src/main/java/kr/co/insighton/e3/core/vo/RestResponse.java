package kr.co.insighton.e3.core.vo;

import java.io.Serializable;

import kr.co.insighton.e3.core.constant.MessageDisplayType;
import lombok.Getter;
import lombok.Setter;

public class RestResponse implements Serializable {
	
	//Private Variables
	private static final long serialVersionUID = -7857710275656843711L;
	
	//data
	@Getter
	@Setter
	private Object data;
	
	@Getter
	@Setter
	private RestResponseMeta meta = new RestResponseMeta();
	
	//Constructor
	public RestResponse() {
		
	}
	
//	@SuppressWarnings("rawtypes")
	public RestResponse(Object data) {
		this.data = data;
		//PageDTO 추가 검토 필요
	}
	
	//Getter & Setter Method
	public void setUserMessage(String message) {
		this.meta.setUserMessage(message);
	}

	public void setSystemMessage(String systemMessage) {
		this.meta.setSystemMessage(systemMessage);
	}
	
	public void setDisplayType(MessageDisplayType displayType) {
		this.meta.setDisplayType(displayType);
	}
	
	public void setCode(String code) {
		this.meta.setCode(code);
	}
	
	//Public Method for chaining
	public RestResponse userMessage(String userMessage) {
		this.setUserMessage(userMessage);
		return this;
	}
	
	public RestResponse systemMessage(String systemMessage) {
		this.setSystemMessage(systemMessage);
		return this;
	}
	
	public RestResponse displayType(MessageDisplayType displayType) {
		this.setDisplayType(displayType);
		return this;
	}
	
	public RestResponse code(String code) {
		this.setCode(code);
		return this;
	}
	
	
	//Inner Class
	@Getter
	@Setter
	private class RestResponseMeta implements Serializable {
		private static final long serialVersionUID = 144476231638185217L;
		
		private String userMessage = "";
		private String systemMessage = "";
		private MessageDisplayType displayType;
		private String code = "";
		
		//page 정보 검토 필요
		
	}

}
