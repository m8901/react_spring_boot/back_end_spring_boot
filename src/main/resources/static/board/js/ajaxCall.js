const request = {
	
	get(url) {
		return fetch(url);	//fetch(url,{method:'GET'});
	},
	
	post(url,payload) {
		return fetch(url,{
			method: 'POST',
			headers:{'content-Type' : 'application/json'},
			body : JSON.stringify(payload)
		})
	},
	
	mpost(url,formData) {
		return fetch(url,{
			method: 'POST',
			headers:{},
			body : formData
		})
	},
		
	patch(url,payload) {
		return fetch(url,{
			method: 'PATCH',
			headers:{'content-Type' : 'application/json'},
			body : JSON.stringify(payload)
		})
	},
	
	mpatch(url,formData) {
		return fetch(url,{
			method: 'PATCH',
			headers:{},
			body : formData
		})
	},
	
	delete(url){
		return fetch(url, {method: 'DELETE'});
	}

}