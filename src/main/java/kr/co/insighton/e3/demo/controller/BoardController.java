package kr.co.insighton.e3.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import kr.co.insighton.e3.demo.domain.Board;
import kr.co.insighton.e3.demo.service.BoardService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/board")
public class BoardController {

	@Autowired
	private BoardService boardService;
	
	@GetMapping
	public List<Board> getList(){
		return boardService.getList();
	}
	
	@GetMapping("/{boardSeq}")
	public Board get(@PathVariable int boardSeq) {
		return boardService.get(boardSeq);
	}
	
	@GetMapping("/save")
	public void save(Board board) {
		boardService.save(board);
	};
	
//	public void update(Board board) {
//		boardService.update(board);
//	};

	@GetMapping("/delete/{boardSeq}")
	public void delelte(@PathVariable int boardSeq) {
		boardService.delelte(boardSeq);
	};
}
