package kr.co.insighton.e3.test.repository;


import java.util.List;


import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import kr.co.insighton.e3.test.DTO.TestBoardDTO;

//@Repository
@Mapper
public interface TestBoardRepository {

	//전체게시글목록
	List<TestBoardDTO> allList();
	
	//게시글 카테고리별 목록
	List<TestBoardDTO> list(String bcategory);
	//게시글 작성
	int addPost(TestBoardDTO testBoardDTO);
	
	//게시글 상세화면
	 TestBoardDTO boardDetail(int bnum); 
	
	//조회수 증가
	void uphit(int bnum);
	
	//게시글 수정
	void modiPost(TestBoardDTO testBoardDTO);
	
	//게시글 삭제
	void delPost(int bnum);
}
