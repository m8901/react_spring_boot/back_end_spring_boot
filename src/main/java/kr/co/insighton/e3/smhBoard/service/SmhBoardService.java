package kr.co.insighton.e3.smhBoard.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.insighton.e3.smhBoard.DTO.SmhBoardDTO;
import kr.co.insighton.e3.smhBoard.DTO.SmhBoardReDTO;
import kr.co.insighton.e3.smhBoard.repository.SmhBoardRepository;

@Service
public class SmhBoardService {
	
	@Autowired
	private SmhBoardRepository smhBoardRepository;
	
	//게시판 전체 목록
	public List<SmhBoardDTO> list(){

		List<SmhBoardDTO> selectAllList = smhBoardRepository.selectAllList();
		
		return selectAllList;
	}
	//카테고리 구분 목록 조회
	public List<SmhBoardReDTO> selectListCate(String boardCate){
		
		List<SmhBoardReDTO> selectListCate = smhBoardRepository.selectListCate(boardCate);
		
		return selectListCate;
	}
	
	
	//게시글 작성
	public void insertBoard(SmhBoardDTO smhBoardDTO) {
		smhBoardRepository.insertBoard(smhBoardDTO);
	}
	
	//게시판 상세보기
	public SmhBoardDTO selectDetail(int boardCode) {
		
		return smhBoardRepository.selectDetail(boardCode);
		
	}

	//게시판 삭제
	public void deleteBoard(int boardCode) {
		
		smhBoardRepository.deleteBoard(boardCode);
	}

	//게시판 수정
	public void updateBoard(SmhBoardDTO smhBoardDTO) {
		
		smhBoardRepository.updateBoard(smhBoardDTO);
	}

	//조회수 증가
	public void updateCnt(int boardCode) {
		
		smhBoardRepository.updateCnt(boardCode);
	}
	
	//------- 댓글 부분 -------\\
	//댓글 등록
	public void insertReply(SmhBoardReDTO smhBoardReDTO) {
		
		smhBoardRepository.insertReply(smhBoardReDTO);
	}
	
	//댓글 조회
	public List<SmhBoardReDTO> replyList(SmhBoardReDTO smhBoardReDTO){
		
		return smhBoardRepository.replyList(smhBoardReDTO);
	}
	
	//댓글 삭제
	public void deleReply(int replyCode) {
		
		smhBoardRepository.deleReply(replyCode);
		

	}
		
}
