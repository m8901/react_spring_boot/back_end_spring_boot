package kr.co.insighton.e3.smhBoard.repository;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import kr.co.insighton.e3.smhBoard.DTO.SmhBoardDTO;
import kr.co.insighton.e3.smhBoard.DTO.SmhBoardReDTO;

@Mapper
public interface SmhBoardRepository {
	
	//게시판 전체 목록
	List<SmhBoardDTO> selectAllList();
	
	//카테고리 구분 목록 조회
	List<SmhBoardReDTO> selectListCate(String boardCate);
	
	//게시판 작성
	void insertBoard(SmhBoardDTO smhBoardDTO);

	//게시판 상세조회
	SmhBoardDTO selectDetail(int boardCode);

	//게시글 삭제
	void deleteBoard(int boardCode);

	//게시글 수정
	void updateBoard(SmhBoardDTO smhBoardDTO);

	//조회수 증가
	void updateCnt(int boardCode);
	
	
	//------- 댓글 부분 -------\\
	//댓글 등록
	void insertReply(SmhBoardReDTO smhBoardReDTO);
	
	//댓글 조회
	List<SmhBoardReDTO> replyList(SmhBoardReDTO smhBoardReDTO);

	//댓글 삭제
	void deleReply(int replyCode);

}
