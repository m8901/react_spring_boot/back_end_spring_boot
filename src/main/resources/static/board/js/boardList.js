const $writeBtn = document.querySelector('.list__writeBtn');


$writeBtn.addEventListener('click',function(){
	window.location.href = '/test/board/write';
})

const $allBtn = document.querySelector('.nav__a');
const $fBtn = document.querySelector('.nav__f');
const $qBtn = document.querySelector('.nav__q');


document?.addEventListener('DOMContentLoaded',function(){
	setNav();
})
function removeNav(){
	$allBtn.classList.remove('board__select');
	$fBtn.classList.remove('board__select');
	$qBtn.classList.remove('board__select');
}

function setNav(){
	removeNav();
	if(location.href.split("/")[4] == "F"){
		$fBtn.classList.add('board__select');
	}else if(location.href.split("/")[4] == "Q"){
		$qBtn.classList.add('board__select');
	}else{
		$allBtn.classList.add('board__select');
	}
}