package kr.co.insighton.e3.bbs.main.domain.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.co.insighton.e3.sys.code.domain.repository.CodeMapper;

@Service
@Transactional
public class MainService {
	
	private final JdbcTemplate jdbcTemplate;
	
	@Autowired
	public MainService(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	

}
