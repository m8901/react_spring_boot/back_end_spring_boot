package kr.co.insighton.e3.smhBoard.DTO;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SmhBoardDTO {
	private int boardCode;
	private String boardTitle;
	private String boardWriter;
	private String boardContent;
	private String boardDate;
	private int boardCnt;
	private String boardCate;
}
