package kr.co.insighton.e3.sys.code.web;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import kr.co.insighton.e3.core.vo.RestResponse;
import kr.co.insighton.e3.sys.code.domain.service.CodeService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class CodeController {
	
	@Autowired
	private CodeService codeService;

	/*-====================================================
	 | VIEW
	 |-====================================================*/
	@GetMapping(value = "/view/codeList")
	public ModelAndView viewTableList() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("sys/code/codeList");
		return modelAndView;
	}

	/*-====================================================
	 | REST API
	 |-====================================================*/
	@GetMapping(value = "/api/sys/code/code-list")
	public RestResponse search(@RequestParam Map<String, Object> param)
		throws Exception {
		return new RestResponse(codeService.codeList(param));
	}
	
	@PostMapping(value = "/api/sys/code/code-read")
	public RestResponse read(@RequestBody Map<String, Object> param)
		throws Exception {
		return new RestResponse(codeService.codeFind(param));
	}
	
	@PostMapping(value = "/api/sys/code/code-create")
	public RestResponse create(@RequestBody Map<String, Object> param) 
		throws Exception {
		return new RestResponse(codeService.create(param));
	}
	
	@PostMapping(value = "/api/sys/code/code-delete")
	public RestResponse delete(@RequestBody Map<String, Object> param)
		throws Exception {
		return new RestResponse(codeService.delete(param));
	}
	
	

}
