package kr.co.insighton.e3.test.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.insighton.e3.test.DTO.TestBoardDTO;
import kr.co.insighton.e3.test.repository.TestBoardRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TestBoardService {

	@Autowired
	private TestBoardRepository testBoardRepository;

	//게시글 전체목록
	public List<TestBoardDTO> list(){
		
		log.info("service");
		List<TestBoardDTO> allList = testBoardRepository.allList();
		
		return allList;
	}
	//게시글 카테고리별 목록
	public List<TestBoardDTO> list(String bcategory){
		
		List<TestBoardDTO> list = testBoardRepository.list(bcategory);
		
		return list;
	}
	//게시글 작성
	public void addPost(TestBoardDTO testBoardDTO) {
		testBoardRepository.addPost(testBoardDTO);
	}
	
	//게시글 상세화면
	public TestBoardDTO boardDetail(int bnum) { 
	  testBoardRepository.uphit(bnum);
	return testBoardRepository.boardDetail(bnum); }
	 
	 
	
	//게시글 수정
	public void modiPost(TestBoardDTO testBoardDTO) {
		testBoardRepository.modiPost(testBoardDTO);
	}
	//게시글 삭제
	public void delPost(int bnum) {
		testBoardRepository.delPost(bnum);
	}
	
}
