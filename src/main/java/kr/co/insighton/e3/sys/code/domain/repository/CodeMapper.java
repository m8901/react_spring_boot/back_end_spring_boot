package kr.co.insighton.e3.sys.code.domain.repository;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CodeMapper {

	public List<Map<String, Object>> codeList(Map<String,Object> param);
	
	public Map<String, Object>codeFind(Map<String,Object> param);
	
	public void createCode(Map<String,Object> param);

	int save(Map<String,Object> param);

	int update(Map<String,Object> param);
	
	int delete(Map<String,Object> param);
}
