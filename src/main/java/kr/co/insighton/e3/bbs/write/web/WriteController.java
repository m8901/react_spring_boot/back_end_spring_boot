package kr.co.insighton.e3.bbs.write.web;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import kr.co.insighton.e3.core.vo.RestResponse;
import kr.co.insighton.e3.bbs.write.domain.service.WriteService;


@RestController
public class WriteController {
	
	@Autowired
	private WriteService writeService;

	/*-====================================================
	 | VIEW
	 |-====================================================*/
	@GetMapping(value = "/bbs/write")
	public ModelAndView viewWriteList() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("bbs/write");
		return modelAndView;
	}

	/*-====================================================
	 | REST API
	 |-====================================================*/
	

}
