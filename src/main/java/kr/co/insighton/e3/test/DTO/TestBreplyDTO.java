package kr.co.insighton.e3.test.DTO;

import lombok.Data;

@Data
public class TestBreplyDTO {

	public int rnum; 	//댓글번호
	public int bnum;	//게시글번호
	public String nickname;	//닉네임
	public String rcdate;	//작성시간
	public String rudate;	//작성시간
	public String rcontent;	//댓글본문
}
