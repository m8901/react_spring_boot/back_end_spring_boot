package kr.co.insighton.e3.bbs.main.web;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import kr.co.insighton.e3.core.vo.RestResponse;
import kr.co.insighton.e3.bbs.main.domain.service.MainService;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class MainController {
	
	@Autowired
	private MainService mainService;

	/*-====================================================
	 | VIEW
	 |-====================================================*/
	@GetMapping(value = "/bbs/main")
	public ModelAndView viewMainList() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("bbs/main");
		return modelAndView;
	}

	/*-====================================================
	 | REST API
	 |-====================================================*/
	

}
